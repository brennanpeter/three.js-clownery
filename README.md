# Three.js Clownery

This repo is for some really stupid three.js ideas I would like to try implementing, if I can get this working it will make for a really dope website to show off.

The goal:

I would like to make a resume website that appears at first to be a really simple resume on an html page. But then after some time the user realizes the resume is a 3d model of a massive city and they are falling towards it in first person. After some time they pull a parachute and glide down in between the buildings. Then I want to parody the intro to the Dying Light game except your goal is to find me in some part of the city and give me a job


Credit to Simon from the youtube channel SimonDev. His tutorial here: https://www.youtube.com/watch?v=oqKzxPMLWxo really helped me start out with using three.js and the starter code for this project is based off of his tutorial


